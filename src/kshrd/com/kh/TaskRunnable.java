package kshrd.com.kh;

public class TaskRunnable implements Runnable {
    @Override
    public void run() {
    }

    void showWord(String text){
        String[] temp = text.split(" ");
        for (String item : temp) {
            System.out.print(item);
            try {
                System.out.print(" ");
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
