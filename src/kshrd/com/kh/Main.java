package kshrd.com.kh;

class TaskWelcome extends Thread{
    final String welcome = "\uD83E\uDD70Hello KSHRD!\uD83D\uDE0D";
    @Override
    public void run() {
       TaskManager.showLetter(welcome);
    }
}

class TaskSymbol1 extends Thread{
    final String symbol1 = "🍏🍏🍏🍏🍏🍏🍏🍏🍏🍏";
    @Override
    public void run() {
        TaskManager.showLetter(symbol1);
    }
}

class TaskExpress extends Thread{
    final String express = "\uD83D\uDCAA I will try my best all the times at KSHRD!";
    @Override
    public void run() {
        TaskManager.showLetter(express);
    }
}

class TaskSymbol2 extends Thread{
    final String symbol2 = "🍎🍎🍎🍎🍎🍎🍎🍎🍎🍎";
    @Override
    public void run() {
        TaskManager.showLetter(symbol2);
    }
}

public class Main{

    public static void main(String[] args) {

        final String download = "Downloading••••••••••Completed 100%!";
        final String thanks = "💖Thank you KSHRD Trainers for providing us the BEST TRAINING EVER!💖";

        TaskWelcome showWelcome = new TaskWelcome();
        TaskSymbol1 showSymbol1 = new TaskSymbol1();
        TaskExpress showExpress = new TaskExpress();
        TaskSymbol2 showSymbol2 = new TaskSymbol2();

        TaskThread taskThread = new TaskThread();
        Thread showDownload = new Thread(){
            @Override
            public void run() {
                taskThread.showText(download,"•");
            }
        };

        TaskRunnable taskRunnable = new TaskRunnable();
        Thread showThanks = new Thread(){
            @Override
            public void run() {
                taskRunnable.showWord(thanks);
            }
        };

        showWelcome.start();
        try {
            showWelcome.join();
        } catch (InterruptedException e) {
            System.out.println(e);
        }

        System.out.println();
        showSymbol1.start();
        try {
            showSymbol1.join();
        } catch (InterruptedException e) {
            System.out.println(e);
        }

        System.out.println();
        showExpress.start();
        try {
            showExpress.join();
        } catch (InterruptedException e) {
            System.out.println(e);
        }

        System.out.println();
        showSymbol2.start();
        try {
            showSymbol2.join();
        } catch (InterruptedException e) {
            System.out.println(e);
        }

        System.out.println();
        showDownload.start();
        try {
            showDownload.join();
        } catch (InterruptedException e) {
            System.out.println(e);
        }

        System.out.println();
        showThanks.start();
        try {
            showThanks.join();
        } catch (InterruptedException e) {
            System.out.println(e);
        }
    }
}
