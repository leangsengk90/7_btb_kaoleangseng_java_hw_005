package kshrd.com.kh;

public class TaskThread extends Thread {
    synchronized void showText(String text, String sign){
        String[] temp = text.split("");
        for (String item : temp) {
            System.out.print(item);
            if (item.equals(sign)) {
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    System.out.println(e);
                }
            }
        }
    }
}
